import re
import os
from collections import defaultdict

cdir = os.getcwd()

def get_crate_arrays():
    crate_array = []
    with open(f"{cdir}/day_e/crates_input.txt") as f:
        for line in f:
            crate_pattern = r'\[(\w)\](?: |\n)|    |\n'
            crate_match = re.findall(crate_pattern, line)
            try:
                assert len(crate_match) == 9
            except AssertionError as e:
                print("Got array < 9 elements. Ignoring it.")
            else:
                crate_array.append(crate_match)
    return crate_array

def organize_crates(crate_matrix):
    crate_dict = defaultdict(list)
    for i in range(len(crate_matrix)-1, -1, -1):
        for j in range(len(crate_matrix[i])):
            crate = crate_matrix[i][j]
            if crate != "":
                crate_dict["crate_col_{0}".format(j+1)].append(crate)
    return crate_dict


def operate_crane(crate_dict):
    with open(f"{cdir}/day_e/procedure_input.txt") as f:
        for line in f:
            procedure_pattern = r'^.*?(\d+).*?(\d+).*?(\d+)'
            procedure_match = re.search(procedure_pattern, line)
            crate_number = int(procedure_match.group(1))
            current_col = int(procedure_match.group(2))
            new_col = int(procedure_match.group(3))
            for i in range(0,crate_number):
                crate = crate_dict["crate_col_{0}".format(current_col)].pop()
                crate_dict["crate_col_{0}".format(new_col)].append(crate)
    return crate_dict

def operate_crane_9001(crate_dict):
    with open(f"{cdir}/day_e/procedure_input.txt") as f:
        for line in f:
            procedure_pattern = r'^.*?(\d+).*?(\d+).*?(\d+)'
            procedure_match = re.search(procedure_pattern, line)
            crate_number = int(procedure_match.group(1))
            current_col = int(procedure_match.group(2))
            new_col = int(procedure_match.group(3))
            python_index_num = crate_number * -1
            grabbed_crates = crate_dict["crate_col_{0}".format(current_col)][python_index_num:]
            remaining_crates = crate_dict["crate_col_{0}".format(current_col)][:python_index_num]
            crate_dict["crate_col_{0}".format(current_col)] = remaining_crates
            crate_dict["crate_col_{0}".format(new_col)] = crate_dict["crate_col_{0}".format(new_col)] + grabbed_crates
            print("test")
    return crate_dict

#crate_dict = operate_crane(organize_crates(get_crate_arrays()))
crate_dict = operate_crane_9001(organize_crates(get_crate_arrays()))
output = ""
for key, value in crate_dict.items():
    if value:
        output += value.pop()
print(output)