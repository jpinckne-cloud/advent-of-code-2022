import re

count = 0
with open("input.txt") as f:
    for line in f:
        elf_pattern = r'^(\d{1,})-(\d{1,}),(\d{1,})-(\d{1,})'
        elf_match = re.search(elf_pattern, line)
        elf_a_low, elf_a_high = (int(elf_match.group(1)), int(elf_match.group(2)))
        elf_b_low, elf_b_high = (int(elf_match.group(3)), int(elf_match.group(4)))
        if elf_a_low >= elf_b_low and elf_a_high <= elf_b_high:
            count += 1
        elif elf_b_low >= elf_a_low and elf_b_high <= elf_a_high:
            count += 1
        else:
            continue
print(count)

#Part two
count = 0
with open("input.txt") as f:
    for line in f:
        elf_pattern = r'^(\d{1,})-(\d{1,}),(\d{1,})-(\d{1,})'
        elf_match = re.search(elf_pattern, line)
        elf_a_low, elf_a_high = (int(elf_match.group(1)), int(elf_match.group(2)))
        elf_b_low, elf_b_high = (int(elf_match.group(3)), int(elf_match.group(4)))
        if elf_a_low in range(elf_b_low, elf_b_high + 1):
            count += 1
        elif elf_b_low in range(elf_a_low, elf_a_high + 1):
            count += 1
print(count)