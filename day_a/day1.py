#Part 1
max_cal = 0
total = 0
with open("input.txt") as f:
    for line in f:
        sanitzied_line = line.replace("\n", "")
        if sanitzied_line == "":
            if max_cal < total:
                max_cal = total
            total = 0
        else:
            total = total + int(sanitzied_line)
    print(max_cal)

#Part 2
max_cals = []
total = 0
sum = 0
with open("input.txt") as f:
    for line in f:
        sanitzied_line = line.replace("\n", "")
        if sanitzied_line == "":
            max_cals.append(total)
            total = 0
        else:
            total = total + int(sanitzied_line)
    max_cals.sort()
    for num in max_cals[len(max_cals)-3:len(max_cals)]:
        sum = sum + num
    print(sum)