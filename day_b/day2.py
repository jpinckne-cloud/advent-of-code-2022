#1 for Rock       A    X
#2 for Paper      B    Y
#3 for Scissors   C    Z
#0 loss           
#3 draw
#6 win

score = 0
with open("input.txt") as f:
    for line in f:
        sanitzied_line = line.replace("\n", "").replace("A", "1").replace("B", "2").replace("C", "3").replace("X", "1").replace("Y", "2").replace("Z", "3")
        if(sanitzied_line[0] == sanitzied_line[2]):
            #Tie
            score = score + int(sanitzied_line[2]) + 3
        elif(int(sanitzied_line[0]) + int(sanitzied_line[2]) == 3):
            if(sanitzied_line[0] == "1"):
                score = score + int(sanitzied_line[2]) + 6
            else:
                score = score + int(sanitzied_line[2]) + 0
        elif(int(sanitzied_line[0]) + int(sanitzied_line[2]) == 4):
            if(sanitzied_line[0] == "1"):
                #Loss Elf Rock beats My Scissors
                score = score + int(sanitzied_line[2]) + 0
            else:
                #Win My Rock beats Elf Scissors
                score = score + int(sanitzied_line[2]) + 6
        elif(int(sanitzied_line[0]) + int(sanitzied_line[2]) == 5):
            if(sanitzied_line[0] == "2"):
                score = score + int(sanitzied_line[2]) + 6
            else:
                score = score + int(sanitzied_line[2]) + 0
print(score)

# Part two
#1 for Rock       A    X
#2 for Paper      B    Y
#3 for Scissors   C    Z
#0 loss           
#3 draw
#6 win

score = 0
with open("input.txt") as f:
    for line in f:
        sanitzied_line = line.replace("\n", "").replace("A", "1").replace("B", "2").replace("C", "3").replace("X", "0").replace("Y", "3").replace("Z", "6")
        if(sanitzied_line[2] == "3"):
            #Tie
            score = score + int(sanitzied_line[0]) + 3
        elif(sanitzied_line[2] == "0"):
            #Loss
            if(sanitzied_line[0] == "1"):
                score = score + int(sanitzied_line[2]) + 3 #scissors
            elif(sanitzied_line[0] == "2"):
                score = score + int(sanitzied_line[2]) + 1 #Rock
            elif(sanitzied_line[0] == "3"):
                score = score + int(sanitzied_line[2]) + 2 #Paper
        elif(sanitzied_line[2] == "6"):
            #Loss
            if(sanitzied_line[0] == "1"):
                score = score + int(sanitzied_line[2]) + 2 #paper
            elif(sanitzied_line[0] == "2"):
                score = score + int(sanitzied_line[2]) + 3 #scissors
            elif(sanitzied_line[0] == "3"):
                score = score + int(sanitzied_line[2]) + 1 #rock
print(score)