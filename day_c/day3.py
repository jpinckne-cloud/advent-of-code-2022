from itertools import islice

sum = 0
priority = 0
with open("input.txt") as f:
    for line in f:
        compartment1 = line[:int(len(line)/2)]
        compartment2 = line[int(len(line)/2):]
        common_letter = list(set(compartment1) & set(compartment2))[0]
        if common_letter.isupper():
            priority = ord(common_letter)-38
        else:
            priority = ord(common_letter)-96
        sum = sum + priority
print(sum)

sum = 0
priority = 0
with open("input.txt") as f:
    while True:
        lines = list(islice(f, 3))
        lines = [x.rstrip('\n') for x in lines]
        if not lines:
            break
        common_letter = list(set(lines[0]) & set(lines[1]) & set(lines[2]))
        if common_letter[0].isupper():
            priority = ord(common_letter[0])-38
        else:
            priority = ord(common_letter[0])-96
        sum = sum + priority
print(sum)